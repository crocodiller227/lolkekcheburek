<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends Controller
{
    /**
     * @Route("/", name="index_action")
     */
    public function indexPageAction()
    {
        return $this->render('index.html.twig');
    }
}